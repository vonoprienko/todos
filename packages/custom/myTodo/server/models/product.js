var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    name: { type : String, required : true},
    price: { type : Number},
    description: { type : String}
});

schema.set('autoIndex', false);
module.exports = mongoose.model('Product', schema);