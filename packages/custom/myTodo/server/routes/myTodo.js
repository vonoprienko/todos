(function () {
  'use strict';

  var express = require('express'),
      fs = require('fs-extra'),
      Product = require('../models/product'),
      router = express.Router();

  /* jshint -W098 */
  // The Package is past automatically as first parameter
  module.exports = function (MyTodo, app, auth, database) {

    app.get('/api/todos', function (req, res, next) {
      res.send('Show all');
    });
    app.post('/api/add', function (req, res, next) {
      res.send('Anyone can access this');
    });

    app.get('/products', auth.requiresLogin, function (req, res, next) {
      Product.find(function (err, products) {
        if (err){
          return next(err)
        }
        console.log(products);
        res.send(products);
      })
    });

    app.post('/', function (req, res, next) {
      Product.create(req.body, function (err, products) {
        if (err) {
          return next(err)
        }

        res.send(products);
      })
    });

    //
    //app.get('/api/myTodo/example/auth', auth.requiresLogin, function (req, res, next) {
    //  res.send('Only authenticated users can access this');
    //});
    //
    //app.get('/api/myTodo/example/admin', auth.requiresAdmin, function (req, res, next) {
    //  res.send('Only users with Admin role can access this');
    //});
    //
    //app.get('/api/myTodo/example/render', function (req, res, next) {
    //  MyTodo.render('index', {
    //    package: 'myTodo'
    //  }, function (err, html) {
    //    //Rendering a view from the Package server/views
    //    res.send(html);
    //  });
    //});
  };
})();
