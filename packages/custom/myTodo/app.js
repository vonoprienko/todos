'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var MyTodo = new Module('myTodo');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
MyTodo.register(function(app, auth, database, system) {

  //We enable routing. By default the Package Object is passed to the routes
  MyTodo.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  MyTodo.menus.add({
    title: 'Link 1',
    link: 'link1',
    roles: ['authenticated','anonymous'],
    menu: 'mymain'
  });
  MyTodo.menus.add({
    title: 'Link 2',
    link: 'link2',
    roles: ['authenticated','anonymous'],
    menu: 'mymain'
  });
  MyTodo.menus.add({
    title: 'link3',
    link: 'myTodo example page',
    roles: ['authenticated','anonymous'],
    menu: 'mymain'
  });


  app.set('views', __dirname + '/server/views');
  MyTodo.aggregateAsset('css', 'myTodo.css');
  MyTodo.angularDependencies(['mean.system']);

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    MyTodo.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    MyTodo.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    MyTodo.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return MyTodo;
});
