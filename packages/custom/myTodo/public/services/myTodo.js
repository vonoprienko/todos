(function () {
  'use strict';

  angular
    .module('mean.myTodo')
    .factory('MyTodo', MyTodo);

  MyTodo.$inject = [];

  function MyTodo() {
    return {
      name: 'myTodo'
    };
  }
})();
