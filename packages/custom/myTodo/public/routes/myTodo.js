(function () {
  'use strict';

  angular
    .module('mean.myTodo')
    .config(myTodo);

  myTodo.$inject = ['$stateProvider'];
  myTodo.$inject = ['$viewPathProvider'];

  function myTodo($viewPathProvider, $stateProvider) {
    $viewPathProvider.override('system/views/index.html', 'myTodo/views/index.html');
  }

})();

//angular.module('mean.mycustompackage', ['mean.system'])
//    .config(['$viewPathProvider', function($viewPathProvider) {
//      $viewPathProvider.override('system/views/index.html', 'mycustompackage/views/myhomepage.html');
//    }]);