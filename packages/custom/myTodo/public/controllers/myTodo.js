(function () {
  'use strict';

  /* jshint -W098 */
  angular
    .module('mean.myTodo')
    .controller('MyTodoController', MyTodoController);

  MyTodoController.$inject = ['$scope', 'Global', 'MyTodo', '$resource'];

  function MyTodoController($scope, Global, MyTodo, $resource) {
    $scope.global = Global;
    $scope.package = {
      name: 'myTodo'
    };
    $scope.hello = 'Hello';

    var Product = $resource('/products/',{}, {'query': {method: 'GET'}});
    refreshList();

    $scope.save = function () {
      Product.save($scope.product, function () {
        refreshList();
        $scope.product = {};
      })
    };

    function refreshList() {
      $scope.products = Product.query();
    }
  }
})();